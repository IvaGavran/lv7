﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Cart cart = new Cart();
            BuyVisitor buyVisitor = new BuyVisitor();
            BorrowVisitor borrowVisitor = new BorrowVisitor();

            VHS vhs = new VHS("New VHS", 100);
            DVD dvd = new DVD("New DVD", DVDType.SOFTWARE, 120);
            Book book = new Book("New Book", 80);

            cart.SetVisitor(borrowVisitor);
            cart.AddItem(vhs);
            cart.AddItem(dvd);
            cart.AddItem(book);

            Console.WriteLine("Final price: " + cart.Accept());
        }
    }
}

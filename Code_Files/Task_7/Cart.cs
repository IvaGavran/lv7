﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_7
{
    class Cart
    {
        private List<IItem> itemList;
        IVisitor visitor;
        public Cart() { itemList = new List<IItem>(); }
        public void SetVisitor(IVisitor visitor)
        {
            this.visitor = visitor;
        }
        public void AddItem(IItem item)
        {
            itemList.Add(item);
        }
        public void RemoveItem(IItem item)
        {
            itemList.Remove(item);
        }
        public double Accept()
        {
            double sum = 0.0;
            foreach (IItem item in itemList)
            {
                sum += item.Accept(visitor);
            }
            return sum;
        }
    }
}

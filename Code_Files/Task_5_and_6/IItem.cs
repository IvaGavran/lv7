﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5_and_6
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5_and_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 5:
            BuyVisitor buyVisitor = new BuyVisitor();
            VHS vhs = new VHS("New VHS", 100);
            DVD dvd = new DVD("New DVD", DVDType.SOFTWARE, 120);
            Book book = new Book("New Book", 80);

            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price plus tax: " + vhs.Accept(buyVisitor));
            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Price plus tax: " + dvd.Accept(buyVisitor));
            Console.WriteLine(book.ToString());
            Console.WriteLine("Price plus tax: " + book.Accept(buyVisitor));

            //Task 6:
            BorrowVisitor borrowVisitor = new BorrowVisitor();
            Console.WriteLine("Borrowing price:");
            Console.WriteLine(vhs.Accept(borrowVisitor));
            Console.WriteLine(dvd.Accept(borrowVisitor));
            Console.WriteLine(book.Accept(borrowVisitor));
        }
    }
}

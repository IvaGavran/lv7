﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5_and_6
{
    //Task 6:
    class BorrowVisitor : IVisitor
    {
        private const double percentage = 0.1;
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
                return Double.NaN;
            return DVDItem.Price * percentage;
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * percentage;
        }
        public double Visit(Book BookItem)
        {
            return BookItem.Price * percentage;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1_and_2
{   
    //Task 2:
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] array, double key);
    }
}
